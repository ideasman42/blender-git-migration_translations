#!/bin/sh

# takes 'git_opti' bare repo and uploads to blender's git server.

rm -rf git_blender_org
mkdir git_blender_org
cp -R git_opti git_blender_org/.git
cd git_blender_org

git remote rm origin
git remote add origin git@git.blender.org:blender-translations

echo "Uploading GIT repo:"
git push --mirror

