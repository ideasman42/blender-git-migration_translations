#!/bin/sh

# this script does the entire conversion, running all other conversion functions.

./convert_pre_svn_sync.sh
./convert_pre_svn_dump.sh

./convert.sh

./convert_post_gc.sh
./convert_post_upload.sh

