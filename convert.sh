#!/bin/sh

if [ A"$REPOSURGEON"A = AA ]; then
    REPOSURGEON=`which reposurgeon`
fi

# Run the conversion
rm -rf git git_opti
time pypy $REPOSURGEON "script translations.lift; prefer git; rebuild git"

# Execute post-conversion git fixups
cd git
sed -n '/^# git/ { s/^# //; p }' <../translations.lift | {
    while read cmd; do
        eval "$cmd"
    done
}

